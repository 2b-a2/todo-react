import React, { Component } from 'react'
import './App.css'
import TodoList from "./components/TodoList"

const uuidv4 = require("uuid/v4")

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [
               {
                 id: uuidv4(),
                 text: "The first item",
                 completed: false
               },
               {
                 id: uuidv4(),
                 text: "The second item",
                 completed: true
               }
             ]
    }
    this.toggleItem = this.toggleItem.bind(this)
    this.addItem = this.addItem.bind(this)
    this.removeItem = this.removeItem.bind(this)
  }

  toggleItem(i) {
    this.setState({
      items: this.state.items.map((item) => (
        item.id === i ? {
                          ...item,
                          completed: !(item.completed)
                        } :
                        { ...item }
      ))
    })
    console.log(`Toggled id ${i}`)
  }

  addItem(item) {
    this.setState({
      items: [
        {
          id: uuidv4(),
          text: item,
          completed: false
        },
        ...this.state.items
      ]
    })
  }

  removeItem(item) {
    this.setState({
      items: this.state.items.filter(({id}) => id !== item)
    })
  }

  render() {
    const {items} = this.state
    return (
      <div>
        <h1 className="header">Todo List</h1>
        <TodoList items={items}
                  itemChecked={(i) => (e) => {
                    this.toggleItem(i)
                  }}
                  itemDeleted={(i) => (e) => {
                    this.removeItem(i)
                  }}
                  addItem={this.addItem}
         />
      </div>
    )
  }
}

export default App

import React from "react"
import TodoItem from "./TodoItem"
import ItemAdder from "./ItemAdder"

const TodoList = ({ items, itemChecked, itemDeleted, addItem }) =>
  <div className="todolist">
    {
      [...items].map((item, i) =>
        <TodoItem key={i}
                  onCheck={itemChecked(item.id)}
                  onDelete={itemDeleted(item.id)}
                  {...item} />
      )
    }
    <ItemAdder addItem={addItem} />
  </div>

export default TodoList

import React from "react"

const ItemAdder = ({ addItem }) => {
  let _newItem;
  return (
    <div className="itemadder">
      <input ref={input => _newItem = input } />
      <button onClick={(e) => {
        addItem(_newItem.value)
        _newItem.value = ""
       } }>Add Item</button>
    </div>
  )
}

export default ItemAdder

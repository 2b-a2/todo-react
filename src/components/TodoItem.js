import React from "react"

const TodoItem = ({ text, completed, onCheck, onDelete }) =>
  <div className="todoitem">
    <label className="todoline">
      <input type="checkbox" checked={completed} onChange={(e) => onCheck(e)} />
      <span className={completed ? "todotext completed" : "todotext"}>{text}</span>
    </label>
    <button className="deletebutton" onClick={(e) => onDelete(e)}>&times;</button>
  </div>

export default TodoItem
